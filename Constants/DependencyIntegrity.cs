﻿namespace RFRocketLibrary.Constants
{
    public static class DependencyIntegrity
    {
        public static string Dapper = "15441d2992a9fe398446367bb406b563";
        public static string Harmony = "a4dbf3fac814e73c56dccaed711fa43c";
        public static string LiteDB = "4092ceeb0be21f4a8bd87daa2188bf09";
        public static string LiteDBAsync = "1203a2b4155bce755c56846680237d55";
        public static string MySqlData = "0ce4244755389d4c955f48d5ee1a994f";
        public static string NewtonsoftJson = "0ed248f9cf0b97fb2f7a307f498d9169";
        // public static string RFRocketLibrary = "";
        public static string SystemManagement = "316e5353bf92251e7de7b67a21baf441";
        public static string SystemRuntimeSerialization = "5e51216f50d9cb1e8fc30fe538e949af";
        public static string SystemXmlLinq = "5b7595869d1270a3b99228bb49170da8";
        public static string UbietyDnsCore = "b93582f37235a8fc7db89b2b69884e57";
    }
}