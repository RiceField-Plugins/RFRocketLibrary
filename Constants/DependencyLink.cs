﻿namespace RFRocketLibrary.Constants
{
    public static class DependencyLink
    {
        public static string Dapper =
            "https://cdn.jsdelivr.net/gh/RiceField-Plugins/LibraryDependencies@main/Dapper.2.0.123.dll";

        public static string Harmony =
            "https://cdn.jsdelivr.net/gh/RiceField-Plugins/LibraryDependencies@main/0Harmony.2.2.0.0.dll";

        public static string LiteDB =
            "https://cdn.jsdelivr.net/gh/RiceField-Plugins/LibraryDependencies@main/LiteDB.5.0.11.0.dll";

        public static string LiteDBAsync =
            "https://cdn.jsdelivr.net/gh/RiceField-Plugins/LibraryDependencies@main/litedbasync.0.0.11.dll";

        public static string MySqlData =
            "https://cdn.jsdelivr.net/gh/RiceField-Plugins/LibraryDependencies@main/MySql.Data.8.0.25.0.dll";

        public static string NewtonsoftJson =
            "https://cdn.jsdelivr.net/gh/RiceField-Plugins/LibraryDependencies@main/Newtonsoft.Json.13.0.1.dll";

        // public static string RFRocketLibrary =
        //     "https://cdn.jsdelivr.net/gh/RiceField-Plugins/LibraryDependencies@latest/RFRocketLibrary.1.0.0.dll";

        public static string SystemManagement =
            "https://cdn.jsdelivr.net/gh/RiceField-Plugins/LibraryDependencies@main/System.Management.4.0.0.0.dll";

        public static string SystemRuntimeSerialization =
            "https://cdn.jsdelivr.net/gh/RiceField-Plugins/LibraryDependencies@main/System.Runtime.Serialization.4.0.0.0.dll";

        public static string SystemXmlLinq =
            "https://cdn.jsdelivr.net/gh/RiceField-Plugins/LibraryDependencies@main/System.Xml.Linq.4.0.0.0.dll";

        public static string UbietyDnsCore =
            "https://cdn.jsdelivr.net/gh/RiceField-Plugins/LibraryDependencies@main/Ubiety.Dns.Core.2.2.1.0.dll";
    }
}