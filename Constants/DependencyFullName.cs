﻿namespace RFRocketLibrary.Constants
{
    public static class DependencyFullName
    {
        public static string Dapper = "Dapper, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null";
        public static string Harmony = "0Harmony, Version=2.2.0.0, Culture=neutral, PublicKeyToken=null";
        public static string LiteDB = "LiteDB, Version=5.0.11.0, Culture=neutral, PublicKeyToken=4ee40123013c9f27";
        public static string LiteDBAsync = "litedbasync, Version=0.0.11.0, Culture=neutral, PublicKeyToken=null";
        public static string MySqlData = "MySql.Data, Version=8.0.25.0, Culture=neutral, PublicKeyToken=c5687fc88969c44d";
        public static string NewtonsoftJson = "Newtonsoft.Json, Version=13.0.0.0, Culture=neutral, PublicKeyToken=30ad4fe6b2a6aeed";
        // public static string RFRocketLibrary = "RFRocketLibrary, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
        public static string SystemManagement = "System.Management, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";
        public static string SystemRuntimeSerialization = "System.Runtime.Serialization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";
        public static string SystemXmlLinq = "System.Xml.Linq, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";
        public static string UbietyDnsCore = "Ubiety.Dns.Core, Version=2.2.1.0, Culture=neutral, PublicKeyToken=c5687fc88969c44d";
    }
}