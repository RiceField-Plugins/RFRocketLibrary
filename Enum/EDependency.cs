﻿namespace RFRocketLibrary.Enum
{
    public enum EDependency
    {
        Dapper,
        Harmony,
        LiteDB,
        LiteDBAsync,
        MySqlData,
        NewtonsoftJson,
        SystemRuntimeSerialization,
        RFRocketLibrary,
        SystemManagement,
        SystemXmlLinq,
        UbietyDnsCore,
    }
}