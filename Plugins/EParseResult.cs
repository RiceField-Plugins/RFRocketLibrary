﻿namespace RFRocketLibrary.Plugins
{
    // Original from https://github.com/ShimmyMySherbet/RocketExtensions
    // Original Author: ShimmyMySherbet
    public enum EParseResult
    {
        Parsed,
        ParseFailed,
        InvalidType,
        PlayerNotFound
    }
}