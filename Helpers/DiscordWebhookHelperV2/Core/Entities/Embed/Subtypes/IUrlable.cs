namespace RFRocketLibrary.Helpers.DiscordWebhookHelperV2.Core.Entities.Embed.Subtypes
{
    public interface IUrlable
    {
        /// <summary>
        ///     Source url.
        /// </summary>
        public string? Url { get; }
    }
}
