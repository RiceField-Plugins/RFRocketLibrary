namespace RFRocketLibrary.Helpers.DiscordWebhookHelperV2.Core.Entities.Embed.Subtypes
{
    public interface IProxyable
    {
        /// <summary>
        ///     A proxied url.
        /// </summary>
        public string? ProxyUrl { get; }
    }
}
