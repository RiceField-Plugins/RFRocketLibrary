using RFRocketLibrary.Helpers.DiscordWebhookHelperV2.Core.Entities.Embed.Subtypes;

namespace RFRocketLibrary.Helpers.DiscordWebhookHelperV2.Core.Entities.Embed
{
    /// <summary>
    ///     Thumbnail of embed.
    /// </summary>
    public interface IEmbedThumbnail : IResizable, IUrlable, IProxyable { }
}
