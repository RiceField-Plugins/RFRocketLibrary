using RFRocketLibrary.Helpers.DiscordWebhookHelperV2.Core.Entities.Embed.Subtypes;

namespace RFRocketLibrary.Helpers.DiscordWebhookHelperV2.Core.Entities.Embed
{
    /// <summary>
    ///     Image of embed.
    /// </summary>
    public interface IEmbedImage : IResizable, IUrlable, IProxyable { }
}
