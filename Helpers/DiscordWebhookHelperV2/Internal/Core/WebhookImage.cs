using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using RFRocketLibrary.Helpers.DiscordWebhookHelperV2.Core.Entities;
using RFRocketLibrary.Helpers.DiscordWebhookHelperV2.Util;
using RFRocketLibrary.Helpers.DiscordWebhookHelperV2.Util.Extensions;

namespace RFRocketLibrary.Helpers.DiscordWebhookHelperV2.Internal.Core
{
    internal struct WebhookImage : IWebhookImage, IEquatable<IWebhookImage?>
    {
        /// <summary>
        ///     An empty image that will not modify the webhook.
        /// </summary>
        public static WebhookImage Empty { get; } = new WebhookImage();

        public ReadOnlyCollection<byte>? Data { get => _data; }

        private readonly ReadOnlyCollection<byte>? _data;
        private string? _uriCached;

        public WebhookImage(byte[] data)
        {
            Checks.CheckForNull(data, nameof(data));
            _data = data.ToReadOnlyCollection()!;
            _uriCached = null;
        }

        public WebhookImage(FileInfo file)
        {
            Checks.CheckForNull(file, nameof(file));
            Checks.CheckForArgument(!file.Exists, nameof(file));
            using var stream = file.OpenRead();
            var temp = new byte[stream.Length];
            stream.Read(temp, 0, temp.Length);
            _data = temp.ToReadOnlyCollection()!;
            _uriCached = null;
        }

        public WebhookImage(Stream stream)
        {
            Checks.CheckForNull(stream, nameof(stream));
            Checks.CheckForArgument(!stream.CanSeek || !stream.CanRead, nameof(stream));

            var temp = new byte[stream.Length - stream.Position];
            stream.Read(temp, 0, temp.Length);
            _data = temp.ToReadOnlyCollection()!;
            _uriCached = null;
        }

        public void Save(string path)
        {
            Checks.CheckForArgument(string.IsNullOrEmpty(path), nameof(path));
            File.WriteAllBytes(path, _data.ToArray());
        }

        public string ToUriScheme()
        {
            if (!string.IsNullOrEmpty(_uriCached))
                return _uriCached!;

            return _uriCached = $"data:image/png;base64,{Convert.ToBase64String(_data.ToArray())}";
        }

        public bool Equals(IWebhookImage? other)
        {
            return !(other is null) && Data == other.Data;
        }
    }
}
