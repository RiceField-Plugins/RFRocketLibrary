namespace RFRocketLibrary.Helpers.DiscordWebhookHelperV2.Action.Entities.Rest.ActionClassified
{
    /// <summary>
    ///     Webhook deletion action.
    /// </summary>
    public interface IDeleteAction : IRestAction { }
}
