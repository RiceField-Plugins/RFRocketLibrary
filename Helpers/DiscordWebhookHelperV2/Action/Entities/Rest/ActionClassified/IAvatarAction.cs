using RFRocketLibrary.Helpers.DiscordWebhookHelperV2.Action.Entities.Rest.ResultClassified;

namespace RFRocketLibrary.Helpers.DiscordWebhookHelperV2.Action.Entities.Rest.ActionClassified
{
    /// <summary>
    ///     Action of getting an webhook image.
    /// </summary>
    public interface IAvatarAction : IRestAction<IAvatarResult> { }
}
