using RFRocketLibrary.Helpers.DiscordWebhookHelperV2.Action.Entities.Rest.ResultClassified;

namespace RFRocketLibrary.Helpers.DiscordWebhookHelperV2.Action.Entities.Rest.ActionClassified
{
    /// <summary>
    ///     Action for getting information about webhook.
    /// </summary>
    public interface IInfoAction : IRestAction<IInfoResult> { }
}
