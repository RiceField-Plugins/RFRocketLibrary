﻿namespace RFRocketLibrary.Helpers.DiscordHelper
{
    public class DiscordAuthor
    {
        public string Id = "";
        public string Username = "";
        public string Discriminator = "";
        public string? Avatar;
        public bool? Bot;
        public bool? System;
        public bool? MfaEnabled;
        public string? Banner;
        public int? Color;
        public string? Locale;
        public bool? Verified;
        public string? Email;
        public int? Flags;
        public int? PremiumType;
        public int? PublicFlags;
    }
}